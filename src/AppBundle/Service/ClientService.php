<?php

namespace AppBundle\Service;

use AppBundle\Entity\Client;
use AppBundle\Entity\ShippingAddress;
use Doctrine\ORM\EntityManager;

class ClientService
{
    protected $clientRepo;
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->clientRepo = $em->getRepository(Client::class);
        $this->em = $em;
    }

    public function getList(): array
    {
        return $this->clientRepo->findAll();
    }

    public function addShippingAddress(Client $client, ShippingAddress $address): ShippingAddress
    {
        $addressCount = $client->getShippingAddresses()->count();

        if ($addressCount >= 3) {
            throw new \InvalidArgumentException('Can add no more than 3 shipping addresses.');
        }

        $client->addShippingAddress($address);
        $this->em->persist($address);

        if ($address->getDefault()) {
            $this->markRestAddressesNotDefault($client, $address);
        } else {
            $this->markFirstAddressAsDefault($client);
        }

        $this->em->flush();

        return $address;
    }

    public function deleteShippingAddress(ShippingAddress $address)
    {
        if ($address->getDefault() === true) {
            throw new \InvalidArgumentException('Cannot delete default shipping address.');
        }
        $this->em->remove($address);
        $this->em->flush($address);
    }

    public function updateShippingAddress(Client $client, ShippingAddress $address): ShippingAddress
    {
        if ($address->getDefault()) {
            $this->markRestAddressesNotDefault($client, $address);
        } else {
            $this->markFirstAddressAsDefault($client);
        }

        $this->em->persist($client);
        $this->em->flush();

        return $address;
    }

    protected function markFirstAddressAsDefault(Client $client)
    {
        $mustMark = true;

        /** @var ShippingAddress $shippingAddress */
        foreach ($client->getShippingAddresses() as $shippingAddress) {
            if ($shippingAddress->getDefault()) {
                $mustMark = false;
                break;
            }
        }

        $mustMark && $client->getShippingAddresses()->first()->setDefault(true);
    }

    protected function markRestAddressesNotDefault(Client $client, ShippingAddress $address)
    {
        /** @var ShippingAddress $shippingAddress */
        foreach ($client->getShippingAddresses() as $shippingAddress) {
            $shippingAddress->getId() !== $address->getId() && $shippingAddress->setDefault(false);
        }
    }

}