<?php

namespace AppBundle\Form\Type;


use AppBundle\Entity\ShippingAddress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShippingAddressType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', TextType::class)
            ->add('city', TextType::class)
            ->add('zipCode', TextType::class)
            ->add('street', TextType::class)
            ->add('default', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No'  => false,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => ShippingAddress::class,
            'csrf_protection' => false,
        ]);
    }

    public function getName()
    {
        return 'shipping_address';
    }
}