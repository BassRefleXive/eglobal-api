<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;

class ClientController extends FOSRestController implements ClassResourceInterface
{
    public function getAction(Client $client)
    {
        return $client;
    }

    public function cgetAction()
    {
        return $this->getDoctrine()->getManager()->getRepository(Client::class)->findAll();
    }
}