<?php

namespace AppBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class BaseController extends FOSRestController
{

    protected function getRequestContentData(Request $request): array
    {
        $data = json_decode($request->getContent(), true);

        if (is_null($data) and $msg = json_last_error_msg()) {
            throw new \InvalidArgumentException($msg);
        }

        return $data;
    }

}