<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Client;
use AppBundle\Entity\ShippingAddress;
use AppBundle\Form\Type\ShippingAddressType;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\RouteResource;


/**
 * @RouteResource("shipping-address")
 */
class ShippingAddressController extends BaseController implements ClassResourceInterface
{

    public function getAction(ShippingAddress $address)
    {
        return $address;
    }

    public function postAction(Client $client, Request $request)
    {
        $form = $this->createForm(ShippingAddressType::class);

        $data = $this->getRequestContentData($request);

        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        $address = $this->get('app_services_clients')->addShippingAddress($client, $form->getData());

        return $address;
    }

    public function putAction(Client $client, ShippingAddress $shippingAddress, Request $request)
    {
        $form = $this->createForm(ShippingAddressType::class, $shippingAddress, [
            'method' => Request::METHOD_PUT,
        ]);

        $data = $this->getRequestContentData($request);

        $form->submit($data);

        if (!$form->isValid()) {
            return $form;
        }

        $address = $this->get('app_services_clients')->updateShippingAddress($client, $form->getData());

        return $address;
    }

    public function deleteAction(Client $client, ShippingAddress $shippingAddress)
    {
        $this->get('app_services_clients')->deleteShippingAddress($shippingAddress);

        return new Response('', Response::HTTP_NO_CONTENT);
    }

}