<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     * @Route("/clients", name="clients")
     * @Route("/clients/{id}", name="client")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }
}
