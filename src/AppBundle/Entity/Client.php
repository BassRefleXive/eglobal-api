<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Client")
 */
class Client
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string")
     */
    protected $lastName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ShippingAddress", mappedBy="client", cascade={"persist"}, orphanRemoval=true)
     */
    protected $shippingAddresses;

    public function __construct()
    {
        $this->shippingAddresses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): Client
    {
        $this->id = $id;

        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): Client
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): Client
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getShippingAddresses(): Collection
    {
        $this->shippingAddresses === null && $this->shippingAddresses = new ArrayCollection();

        return $this->shippingAddresses;
    }

    public function addShippingAddress(ShippingAddress $shippingAddress): Client
    {
        $this->getShippingAddresses()->add($shippingAddress->setClient($this));

        return $this;
    }

    public function removeShippingAddress(ShippingAddress $shippingAddress): Client
    {
        $items = $this->getShippingAddresses();

        if (($index = $items->indexOf($shippingAddress)) !== false) {
            $items->remove($index);
        }

        return $this;
    }

}