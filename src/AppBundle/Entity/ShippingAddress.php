<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="ShippingAddress")
 */
class ShippingAddress
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $country;

    /**
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @ORM\Column(type="string")
     */
    protected $zipCode;

    /**
     * @ORM\Column(type="string")
     */
    protected $street;

    /**
     * @ORM\Column(type="boolean", name="`default`")
     */
    protected $default;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Client", inversedBy="shippingAddresses")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id): ShippingAddress
    {
        $this->id = $id;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(string $country): ShippingAddress
    {
        $this->country = $country;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity(string $city): ShippingAddress
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode()
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): ShippingAddress
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function setStreet(string $street): ShippingAddress
    {
        $this->street = $street;

        return $this;
    }

    public function getDefault()
    {
        return $this->default;
    }

    public function setDefault($default): ShippingAddress
    {
        $this->default = !!$default;

        return $this;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(Client $client): ShippingAddress
    {
        $this->client = $client;

        return $this;
    }

}